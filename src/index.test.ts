import Pokedex from "./index";

it("list all pokemons", () => {
  expect(Pokedex().listPokemons()).toEqual(["Charmander"]);
});
