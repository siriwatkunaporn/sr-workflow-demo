# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.0.1-rc.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v3.0.0...v3.0.1-rc.0) (2020-10-17)

## [3.0.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v3.0.0-rc.3...v3.0.0) (2020-10-13)

## [3.0.0-rc.3](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v3.0.0-rc.2...v3.0.0-rc.3) (2020-10-13)

## [3.0.0-rc.2](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v3.0.0-rc.1...v3.0.0-rc.2) (2020-10-13)

## [3.0.0-rc.1](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v3.0.0-rc.0...v3.0.0-rc.1) (2020-10-13)

## [3.0.0-rc.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.1.0-rc.0...v3.0.0-rc.0) (2020-10-12)


### ⚠ BREAKING CHANGES

* we don't need to use breaking_change var anymore

### Bug Fixes

* disable breaking ([ddc62b6](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/ddc62b69ab960de0da88ba3ee452921733d46139))

## [2.1.0-rc.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.2-rc.0...v2.1.0-rc.0) (2020-10-12)


### Features

* update const ([e180177](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/e18017746502778e8f93a5ef997e900fcbc2ef79))

### [2.0.2-rc.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.1...v2.0.2-rc.0) (2020-10-12)

### [2.0.1](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.1-rc.0...v2.0.1) (2020-10-12)

### [2.0.1-rc.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.0-rc.6...v2.0.1-rc.0) (2020-10-12)


### Bug Fixes

* feat-4 ([3b8c621](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/3b8c6219934ab17ba3a88c46510d595c89878a22))

## [2.0.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.0-rc.6...v2.0.0) (2020-10-12)

## [2.0.0-rc.6](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.0-rc.5...v2.0.0-rc.6) (2020-10-12)

## [2.0.0-rc.5](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.0-rc.4...v2.0.0-rc.5) (2020-10-12)

## [2.0.0-rc.4](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.0-rc.3...v2.0.0-rc.4) (2020-10-12)


### Features

* feat-3 ([447b743](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/447b7433115e2dd0750207d4be5d71f9830cf37c))

## [2.0.0-rc.3](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.0-rc.2...v2.0.0-rc.3) (2020-10-12)


### Features

* update const ([f0df88b](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/f0df88b07104b3fdbb224fe1e1450082e8cc0ebc))

## [2.0.0-rc.2](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.0-rc.1...v2.0.0-rc.2) (2020-10-12)


### Features

* update constant ([8dc8ff2](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/8dc8ff2426f8fd6a618468856863a0ff53f8caaa))

## [2.0.0-rc.1](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.0-rc.0...v2.0.0-rc.1) (2020-10-12)

## [2.0.0-rc.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v1.1.0-rc.4...v2.0.0-rc.0) (2020-10-12)


### ⚠ BREAKING CHANGES

* Change something

### Features

* break ([fce3497](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/fce3497994c4be20765b11431bb14a2ec83ded1c))

## [1.1.0-rc.4](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v1.1.0-rc.3...v1.1.0-rc.4) (2020-10-11)

## [1.1.0-rc.3](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v1.1.0-rc.2...v1.1.0-rc.3) (2020-10-11)

## [1.1.0-rc.2](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v1.1.0-rc.1...v1.1.0-rc.2) (2020-10-11)

## 1.1.0-rc.1 (2020-10-11)


### Features

* **constant:** add constant ([bd2ad38](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/bd2ad38fbf5651e85a398dce72d8f2273adf36fd))


### Bug Fixes

* rename "Charmander" to "Charmandear" ([ba60213](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/ba60213e12732d3ecaacddddabecadb787683209))

## 1.1.0-rc.0 (2020-10-11)


### Features

* **constant:** add constant ([bd2ad38](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/bd2ad38fbf5651e85a398dce72d8f2273adf36fd))


### Bug Fixes

* rename "Charmander" to "Charmandear" ([ba60213](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/ba60213e12732d3ecaacddddabecadb787683209))

### 1.0.1 (2020-10-11)


### Bug Fixes

* rename "Charmander" to "Charmandear" ([ba60213](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/ba60213e12732d3ecaacddddabecadb787683209))
